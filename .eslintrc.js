module.exports = {
  "extends": "airbnb-base",
  "rules": {
    "import/no-extraneous-dependencies": [
      "error",
      {
        "devDependencies": [
          "**/__tests__/*.js",
          "**/__mocks__/*.js"
        ],
      },
    ],
  },
  "env": {
    "node": true,
    "jest": true,
    "jest/globals": true,
  },
  "plugins": [
    "jest",
  ],
};
