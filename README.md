# power-markdown

Power-makdown (as in PowerPoint) aims to be a tool to build presentations whose
content is mainly code. Some of the feature (on TODO):

## Sane CLI. No boilerplates.

There're only 2 useful commands right now:

* `powermd init` it creates the `slides` folder for you (useful, eh?)
* `powermd watch` it builds your slides and serves it on
  `http://localhost:999`. Oh, and it is hot reload ready.

## Write slides on markdown

Yup, As easy as that. 1 slide per markdown file.

    # Hello world

    ```javascript
    const asdf = 'hello world';
    const fun = (arg1, arg2) => {
      const other = arg1 - arg2;
      return arg1 * other;
    };

    console.log(fun());
    ```

gets converted into:

![](sample.png)

Yup, code is higlighted! ...only for javascript right now... more languages in
their way!

# TODO!

* terminal highlighting
* Support for embeded gists sharing
