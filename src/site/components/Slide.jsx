import React from 'react';
import PropTypes from 'prop-types';
import remark from 'remark';
import reactRenderer from 'remark-react';
import RemarkLowlight from 'remark-react-lowlight';

import javascript from 'highlight.js/lib/languages/javascript';
import merge from 'deepmerge';
import sanitizeGhSchema from 'hast-util-sanitize/lib/github.json';

import styles from './Slide.scss';

const schema = merge(sanitizeGhSchema, {
  attributes: {
    code: ['className'],
  },
});

function markdownToReact(markdown) {
  return remark().use(reactRenderer, {
    sanitize: schema,
    remarkReactComponents: {
      code: RemarkLowlight({
        javascript,
      }),
    },
  }).processSync(markdown).contents;
}

const Slide = ({ markdown }) => (
  <div className={styles.container}>
    <div className={styles.left}> </div>
    <div className={styles.slide}>
      {markdownToReact(markdown)}
    </div>
    <div className={styles.right}> </div>
  </div>
);

Slide.propTypes = {
  markdown: PropTypes.string.isRequired,
};

export default Slide;
