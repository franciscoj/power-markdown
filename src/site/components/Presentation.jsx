import React from 'react';
import PropTypes from 'prop-types';
import Mousetrap from 'mousetrap';
import Slide from './Slide.jsx';
import styles from './Presentation.scss';

class Presentation extends React.Component {
  constructor(props) {
    super(props);

    this.state = { slideIndex: 0 };
  }

  nextSlide() {
    const { fileNames } = this.props;
    const { slideIndex } = this.state;

    if (slideIndex >= fileNames.length - 1) {
      this.setState({ slideIndex: 0 });
    } else {
      this.setState({ slideIndex: slideIndex + 1 });
    }
  }

  prevSlide() {
    const { fileNames } = this.props;
    const { slideIndex } = this.state;

    if (slideIndex === 0) {
      this.setState({ slideIndex: fileNames.length - 1 });
    } else {
      this.setState({ slideIndex: slideIndex - 1 });
    }
  }

  componentWillMount() {
    Mousetrap.bind('right', this.nextSlide.bind(this));
    Mousetrap.bind('left', this.prevSlide.bind(this));
  }

  render() {
    const { fileNames, markdownReader } = this.props;
    const { slideIndex } = this.state;
    const fileName = fileNames[slideIndex];

    return (
      <div className={styles.presentation}>
        <Slide key={fileName} markdown={markdownReader(fileName)} />
      </div>
    );
  }
}

Presentation.propTypes = {
  fileNames: PropTypes.array.isRequired,
  markdownReader: PropTypes.func.isRequired,
};

export default Presentation;
