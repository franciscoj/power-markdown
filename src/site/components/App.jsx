import React from 'react';
import Presentation from './Presentation.jsx';

const markdownReader = require.context(
  'slides',
  true,
  /\.md$/,
);

const App = () => (
  <Presentation
    fileNames={markdownReader.keys()}
    markdownReader={markdownReader}
  />
);

export default App;
