const chalk = require('chalk');

/**
 * Interface around `console.log` to provide colored logging.
 */
class Log {
  constructor(logger = console) {
    this.logger = logger;
  }

  log(...args) {
    this.logger.log(...args);
  }
  /**
   * Logs something to the stdout with a green bold style
   * @param {string} string - the text to log.
   */
  success(string) {
    this.log(chalk.green.bold(string));
  }
  /**
   * Logs something to the stdout with a yellow style
   * @param {string} string - the text to log.
   */
  warning(string) {
    this.log(chalk.yellow(string));
  }
  /**
   * Logs something to the stdout with a red style
   * @param {string} string - the text to log.
   */
  danger(string) {
    this.log(chalk.red(string));
  }
}

module.exports = Log;
