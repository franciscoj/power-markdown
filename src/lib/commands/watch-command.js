const Server = require('webpack-dev-server');
const webpack = require('webpack');
const webpackConf = require('../webpack.conf.js');

module.exports = {
  command: 'watch',
  describe: 'watch the presentation + build + server',
  builder: {},

  /**
   * Initializes a folder by creating the basic skeleton.
   *
   * @param {Object} _argv - Arguments passed to the command.
   */
  handler() {
    const compiler = webpack(webpackConf);
    const server = new Server(compiler, {
      hot: true,
      stats: {
        colors: true,
      },
    });
    server.listen(9999);
  },
};

