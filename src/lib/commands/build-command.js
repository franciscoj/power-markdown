const webpack = require('webpack');
const webpackConf = require('../webpack.conf.js');

module.exports = {
  command: 'build',
  describe: 'Builds the presentation',
  builder: {},

  /**
   * Initializes a folder by creating the basic skeleton.
   *
   * @param {Object} _argv - Arguments passed to the command.
   */
  handler() {
    const compiler = webpack(webpackConf);
    compiler.run((error, stats) => {
      if (error) {
        console.info(error);
        return;
      }

      console.log(stats.toString({
        chunks: false,
        colors: true,
      }));
    });
  },
};
