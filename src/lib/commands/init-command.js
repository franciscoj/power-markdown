const InitAction = require('../command_actions/init-action');

/**
 * Command to initialize the current folder as a powermd project. This is just
 * an object to interface with YARGS.
 *
 * @see {@link ../command_actions/init-action.js}
 */
module.exports = {
  command: 'init',
  describe: 'Creates a presentation skeleton in the current folder.',
  builder: {},

  /**
   * Initializes a folder by creating the basic skeleton.
   *
   * @param {Object} _argv - Arguments passed to the command.
   */
  handler() {
    new InitAction().run();
  },
};
