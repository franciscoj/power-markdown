const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: [
    require.resolve('react-hot-loader/patch'),
    `${require.resolve('webpack-dev-server/client')}?http://localhost:9999`,
    require.resolve('webpack/hot/only-dev-server'),
    path.resolve(__dirname, '../site/index.jsx'),
  ],

  output: {
    path: path.resolve(process.cwd(), 'build'),
    filename: 'index.js',
    publicPath: '/',
  },

  devtool: 'inline-source-map',

  devServer: {
    hot: true,
    path: path.resolve(process.cwd(), 'build'),
    publicPath: '/',
  },

  module: {
    rules: [
      {
        test: /\.md$/,
        use: require.resolve('raw-loader'),
      },
      {
        test: /\.jsx?$/,
        use: {
          loader: require.resolve('babel-loader'),
          options: {
            presets: [
              ['es2015', { modules: false }],
              // webpack understands the native import syntax, and uses it for tree shaking

              'react',
              // Transpile React components to JavaScript
            ],
            plugins: [
              'react-hot-loader/babel',
              // Enables React code to work with HMR.
            ],
          },
        },
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: require.resolve('style-loader'),
          },
          {
            loader: require.resolve('css-loader'),
            options: {
              modules: true,
              sourceMap: true,
            },
          },
          {
            loader: require.resolve('sass-loader'),
            options: {
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },

  resolve: {
    alias: {
      slides: path.resolve(process.cwd(), 'slides'),
    },
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new HtmlWebpackPlugin({
      title: 'My slides',
      filename: path.resolve(process.cwd(), 'build/index.html'),
      template: path.resolve(__dirname, '../../node_modules/html-webpack-template/index.ejs'),
      appMountId: 'root',
      inject: 'body',
    }),
  ],
};
