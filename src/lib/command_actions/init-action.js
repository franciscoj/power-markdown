const Log = require('../log');
const mkdirP = require('../mkdir-p');

/**
 * Initializes the skeleton for a new presentation. For now it is just the
 * folder `slides`.
 *
 * In case the folder doesn't exist logs it to the console and exists.
 */
class InitAction {
  constructor(log = new Log()) {
    this.log = log;
  }

  /**
   * Performs the creation.
   *
   * @return {Promise}
   */
  run() {
    return mkdirP('slides')
      .then(() => this.log.success("Created folder: 'slides'"))
      .catch((error) => {
        if (error) {
          this.log.danger("Error creating: 'slides'");
          return;
        }

        this.log.warning("Existing folder: 'slides'");
      });
  }
}

module.exports = InitAction;
