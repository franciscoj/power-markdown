const fs = require('fs');
const tmp = require('tmp');
const InitAction = require('../init-action.js');
const Log = require('../../log');

const chalk = require('chalk');

class MockConsole {
  constructor() {
    this.buffer = [];
  }

  log(...strings) {
    this.buffer = [
      ...this.buffer,
      ...strings,
    ];
  }

  flush() {
    const output = this.buffer;
    this.buffer = [];

    return output;
  }
}

describe('InitCommand', () => {
  describe('#run', () => {
    let output;
    let action;
    let mockConsole;
    let cwd;
    const oldCwd = process.cwd();

    beforeEach(() => {
      mockConsole = new MockConsole();
      const log = new Log(mockConsole);
      action = new InitAction(log);
      cwd = tmp.dirSync().name;
      process.chdir(cwd);
    });

    afterEach(() => {
      process.chdir(oldCwd);
    });

    describe('when the skeleton does not exist', () => {
      it('creates skeleton folders', () =>
        action.run()
          .then(() => {
            output = mockConsole.flush()[0];
            expect(output).toBe(chalk.green.bold("Created folder: 'slides'"));
          }),
      );
    });

    describe('when the skeleton exists', () => {
      beforeEach(() => {
        fs.mkdirSync('slides');
      });

      it('does not create the folder', () =>
        action.run()
          .catch(() => {
            output = mockConsole.flush()[0];
            expect(output).toBe(chalk.yellow("Existing folder: 'slides'"));
          }),
      );
    });
  });
});
