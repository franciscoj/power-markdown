const { VERSION } = require('../powermd-info');
const yargs = require('yargs/yargs');
const InitCommand = require('./commands/init-command');
const BuildCommand = require('./commands/build-command');
const WatchCommand = require('./commands/watch-command');

class Cli {
  constructor() {
    this.parser = yargs()
      .usage('Usage $0 <command> [options]')
      .command(InitCommand)
      .command(BuildCommand)
      .command(WatchCommand)
      .version(VERSION)
      .help('h')
      .alias('h', 'help');
  }

  run({ argv }) {
    const args = this.parser.parse(argv);

    if (!args) { this.onFail(); }
  }

  onFail() {
    this.parser.showHelp();
  }
}

module.exports = Cli;
