const fs = require('fs');

/**
 * Create a folder as mkdir -p would do.
 * @param {string} path - the path to the folder to create.
 * @return {Promise} the promise is rejected in case the folder already exists
 * or an error happens. If an error happens reject receives an error parameter.
 */
function mkdirP(path) {
  return new Promise((resolve, reject) => {
    fs.stat('slides', (statError, stat) => {
      if (statError && statError.code === 'ENOENT') {
        fs.mkdir(path, '755', (mkdirError) => {
          if (mkdirError) {
            reject();
            return;
          }

          resolve();
        });
        return;
      }

      if (stat && stat.isDirectory()) {
        reject();
        return;
      }

      resolve();
    });
  });
}

module.exports = mkdirP;
