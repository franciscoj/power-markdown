const { version: VERSION } = require('../package.json');

module.exports = {
  VERSION,
};
